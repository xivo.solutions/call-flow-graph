# Graph project with Angular

This project is a demonstration of creating a chart using Angular and a charting library such as D3js. It shows how to set up a chart component to display data as a chart. It is linked with an API: https://gitlab.com/xivo.solutions/call-flow-api.git

## Screenshot

![Example](./src/assets/Example.png "Example")

## Facility

1. Clone this repository to your local machine:

```bash
git clone https://gitlab.com/xivo.solutions/call-flow-api.git
```
Navigate to the project directory:
```bash
cd call-flow-api
```
Install dependencies:
```bash
npm install
```

### Use
Change the URL (your-ip) of the 'src\app\API_call\data-import.service.ts' file to your api server if it is different or put localhost:

```bash
private _hostName: string = 'http://{your-ip}:9000';
```

Run the project using the following command:

```bash
ng serve
```
Open your browser and navigate to http://{your-ip}:4200 to see your application in action.

### Chart Customization
The chart is based on the D3js library. You can customize the chart type, displayed data, colors and other settings by editing the chart component in src/app/visuals/graph/graph.component.ts.
