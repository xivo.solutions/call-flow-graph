import { CUSTOM_ELEMENTS_SCHEMA, LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GraphComponent } from './visuals/graph/graph.component';
import { NodeVisualComponent } from './visuals/graph/node-visual/node-visual.component';
import { LinkVisualComponent } from './visuals/graph/link-visual/link-visual.component';
import { D3Service } from './d3';
import { D3_DIRECTIVES } from './d3/directives';
import { DataImportService } from './API_call/data-import.service';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatePickerComponent } from "./visuals/side-band/date-picker/date-picker.component";
import { LegendComponent} from './visuals/side-band/legend/legend.component';
import { CommonModule, registerLocaleData} from '@angular/common';
import { FormsModule} from '@angular/forms';
import { MatSliderModule} from "@angular/material/slider";
import { NodeSelectorComponent } from './visuals/side-band/node-selector/node-selector.component';
import { MatCheckboxModule} from "@angular/material/checkbox";
import { FilterPipe} from "./visuals/side-band/node-selector/filter.pipe";
import { MatInputModule } from '@angular/material/input';
import { SideBandComponent } from "./visuals/side-band/side-band.component";
import { MatIconModule } from '@angular/material/icon';
import { SideBandService } from './visuals/side-band/side-band.service';

//registerLocaleData(localeFr, 'fr');

@NgModule({
    declarations: [
        AppComponent,
        GraphComponent,
        LegendComponent,
        NodeVisualComponent,
        LinkVisualComponent,
        ...D3_DIRECTIVES,
        NodeSelectorComponent,
        FilterPipe,
        SideBandComponent,
    ],
    providers: [
        D3Service,
        DataImportService,
        SideBandService,
        LegendComponent,
        NodeSelectorComponent,
        { provide: LOCALE_ID, useValue: 'fr'}
    ],
    bootstrap: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    DatePickerComponent,
    CommonModule,
    FormsModule,
    MatSliderModule,
    MatCheckboxModule,
    MatInputModule,
    MatIconModule
  ],
    schemas: [
      CUSTOM_ELEMENTS_SCHEMA
    ]
})
export class AppModule { }
