import { ChangeDetectorRef, Component, HostListener } from '@angular/core';
import { DataImportService } from 'app/API_call/data-import.service';
import { Import, Mds } from 'app/API_call/models';
import { Link } from 'app/d3/models/link';
import { Node } from 'app/d3/models/node';
import { ForceDirectedGraph } from 'app/d3/models/force-directed-graph';
import { D3Service } from 'app/d3/d3.service';
import { GraphService } from './graph.service';


@Component({
  selector: 'app-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})

export class GraphComponent {
  nodes: Node[];
  links: Link[];




  graph!: ForceDirectedGraph;

  constructor(private d3Service: D3Service,
    private ref: ChangeDetectorRef,
    private _dataImportService : DataImportService,
    private _graphService : GraphService) {  }


  ngOnInit() {
    this._dataImportService.subscribeToImport().subscribe((data : Import | undefined) => {
      if (data) {
        this._dataImportService.subscribeToExtremums().subscribe(extremums => {
            const maxNodeCalls = extremums.maxNodeCalls;
            const minNodeCalls = extremums.minNodeCalls;
            const maxLinkCalls = extremums.maxLinkCalls;
            const minLinkCalls = extremums.minLinkCalls;
            const maxUsers = extremums.maxUsers;
            const minUsers = extremums.minUsers;
            this.nodes=[]
            this.links=[]

            data.mdsList.forEach(element => {
              this.nodes.push(new Node(element.name, element.display_name, 'ellipse', this._graphService.nodeColorFromCalls(element.calls, maxNodeCalls, minNodeCalls),this._graphService.nodeSize(element.users, maxUsers, minUsers, element.display_name), element.users));
            });
            var i : number = 0;
            data.callDatasList.forEach(element => {
              this.links.push(new Link(i, this.nodes, element.caller_mds, element.callee_mds, this._graphService.linkColorFromCalls(element.calls, maxLinkCalls, minLinkCalls), this._graphService.linkSizeFromCalls(element.calls, maxLinkCalls, minLinkCalls), element.calls.toString(), element.calls));
              i++;
            });

            data.trunkList.forEach(element => {
              this.nodes.push(new Node(element.name, element.name, 'rectangle', this._graphService.nodeColorFromCalls(element.incoming + element.outgoing, maxNodeCalls, minNodeCalls) , {x : element.name.length*12, y : 45}, 0, element.incoming, element.outgoing, element.mds, (data.mdsList.find(mds =>mds.name == element.mds) as Mds).users as number));
              if (element.incoming!=0) {

                this.links.push(new Link(i, this.nodes, element.mds, element.name, this._graphService.linkColorFromCalls(element.incoming, maxLinkCalls, minLinkCalls), this._graphService.linkSizeFromCalls(element.incoming, maxLinkCalls, minLinkCalls), element.incoming.toString(), element.incoming))
                i++;
              }

              if (element.outgoing!=0) {
                this.links.push(new Link(i, this.nodes, element.name, element.mds, this._graphService.linkColorFromCalls(element.outgoing, maxLinkCalls, minLinkCalls), this._graphService.linkSizeFromCalls(element.outgoing, maxLinkCalls, minLinkCalls), element.outgoing.toString(), element.outgoing))
                i++;
              }
            });
            /** Receiving an initialized simulated graph from our custom d3 service */


            this.graph = this.d3Service.getForceDirectedGraph(this.nodes, this.links);

            /** Binding change detection check on each tick
             * This along with an onPush change detection strategy should enforce checking only when relevant!
             * This improves scripting computation duration in a couple of tests I've made, consistently.
             * Also, it makes sense to avoid unnecessary checks when we are dealing only with simulations data binding.
             */
            this.graph.ticker.subscribe((d) => {
              this.ref.markForCheck();
            });
          })
      }
  })
}

  ngAfterViewInit() {
    if (this.graph) {
      this.graph.initSimulation();
    }
  }


}
