import { TestBed } from '@angular/core/testing';

import { LinkVisualService } from './link-visual.service';

describe('LinkVisualService', () => {
  let service: LinkVisualService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LinkVisualService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
