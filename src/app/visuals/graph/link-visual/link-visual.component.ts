import { Component, Input } from '@angular/core';
import { Link } from 'app/d3';
import * as d3 from 'd3';
import { LinkVisualService } from './link-visual.service';
@Component({
  selector: '[linkVisual]',
  templateUrl : './link-visual.component.html',
  styleUrls: ['./link-visual.component.scss']
})
export class LinkVisualComponent  {
  @Input('linkVisual') link : Link;

  constructor(private _linkVisualService : LinkVisualService) {}

  ngOnInit() {
    const markerId = `arrowhead_${this.link.id}`;
    const existingMarker = d3.select(`defs #${markerId}`);

    if (existingMarker.node()) {
      // If the marker with the same ID exists, remove it
      existingMarker.remove();
    }

    d3.select("defs")
      .append("svg:marker")
      .attr("id", markerId)
      .attr("viewBox", "0 -5 10 10")
      .attr("refX", 6)
      .attr("orient", "auto")
      .append("svg:path")
      .attr("d", "M0,-5L10,0L0,5")
      .style("fill", this.link.color);
  }

  get path() : string {
    var drx = 0;
    var dry = 0;
    var selfLinked : Boolean
    this._linkVisualService.initSourceNodePosition(this.link);
    this._linkVisualService.initTargetNodePosition(this.link);

    if (this.link.target == this.link.source) {
      this._linkVisualService.selfLinkPosition(this.link.target.size, this.link.size);
      drx = dry = this.link.size*4;
      selfLinked = true;
    }
    else {
      this._linkVisualService.setSourceLinkPosition(this.link.source);
      this._linkVisualService.setTargetLinkPosition(this.link.target, this.link.size);
      selfLinked = false;
    }
    return this._linkVisualService.constructPath(selfLinked, drx, dry)
  }

  get arrowHeadMarkerName() {
    return this._linkVisualService.getArrowHeadMarkerName(this.link)
  }

  get textCoordinates() {
    return this._linkVisualService.textCoordinates
  }

  get display() {
    return this._linkVisualService.display(this.link)
  }
}
