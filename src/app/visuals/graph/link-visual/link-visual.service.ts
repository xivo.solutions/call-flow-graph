import { Injectable } from '@angular/core';
import { LegendService } from 'app/visuals/side-band/legend/legend.service';
import { NodeSelectorService } from 'app/visuals/side-band/node-selector/node-selector.service';
import { NodeVisualService } from '../../graph/node-visual/node-visual.service';
import { Link, Node } from 'app/d3';
import { Vector } from 'app/d3/models/vector.model';

@Injectable({
  providedIn: 'root'
})
export class LinkVisualService {

  // coordinates of the center of the source / destination Node
  _y1 : number = 0;
  _x1 : number = 0;
  _y2 : number = 0;
  _x2 : number = 0;

  // coordinates of the source / target of the arrow
  y1 : number = 0;
  x1 : number = 0;
  y2 : number = 0;
  x2 : number = 0;

  get GAP() {return 0.35}

  constructor(private _legendService : LegendService, private _nodeSelectorService : NodeSelectorService, private _nodeVisualService : NodeVisualService) { }

  initSourceNodePosition(link : Link){
    if (link && link.source && link.source.x && link.source.y) {
      this._x1 = link.source.x;
      this._y1 = link.source.y;
    }
  }

  initTargetNodePosition(link : Link){
    if (link && link.target && link.target.x && link.target.y) {
      this._x2 = link.target.x;
      this._y2 = link.target.y;
    }
  }

  selfLinkPosition(size : Vector, linksize : number) {
    this.x1 = this._x1;
    this.y1 = this._y1 - size.y;
    this.x2 = this._x1 + size.x + linksize/2;
    this.y2 = this._y1;
  }

  get alpha() {
    return Math.atan2(this._y2-this._y1, this._x2-this._x1);
  }

  setTargetLinkPosition (node : Node, linkSize : number) {
    if (node.isMds) {
      this.x2 = this._x2 - (node.size.x + linkSize) * Math.cos(this.alpha + this.GAP);
      this.y2 = this._y2 - (node.size.y + linkSize) * Math.sin(this.alpha + this.GAP);
    }
    if (node.isTrunk){
      const sgn = Math.sign(this._y2-this._y1);
      this.x2 = this._x2 + sgn * node.size.x/4;
      this.y2 = this._y2 - sgn * (node.size.y/2) - Math.sin(this.alpha)*linkSize;
    }
  }

  setSourceLinkPosition (node :Node) {
    if (node.isMds) {
      this.x1 = this._x1 + (node.size.x) * Math.cos(this.alpha - this.GAP);
      this.y1 = this._y1 + (node.size.y) * Math.sin(this.alpha - this.GAP);
    }

    if (node.isTrunk){
      var sgn = Math.sign(this._y2-this._y1);
      this.x1 = this._x1 + sgn * node.size.x/4;
      this.y1 = this._y1 + sgn * node.size.y/2;
    }
  }

  getArrowHeadMarkerName(link : Link) : string {
    return `url(#arrowhead_${link.id})`
  }

  getUnitaryNormalVector() {
    function normalize(vect : {x : number, y : number}) {
      return {x : vect.x/(Math.sqrt(vect.x**2 + vect.y**2)), y : vect.y/(Math.sqrt(vect.x**2 + vect.y**2))};
    }
    return normalize({x : (this.y1-this.y2),y : (this.x2-this.x1)});
  }

  get textCoordinates() : {x : number, y : number} {
    const x : number = (this.x1+this.x2)/2;
    const y : number = (this.y1+this.y2)/2;

    const unitaryNormalVector = this.getUnitaryNormalVector()
    const offset_x  = - unitaryNormalVector.x * 25;
    const offset_y = - unitaryNormalVector.y * 25;

    return { x : x + offset_x, y : y + offset_y}
  }

  constructPath(self : Boolean, drx : number , dry : number): string {
    if (self){
      return `M${this.x1},${this.y1}A${drx},${dry} 0,1,1 ${this.x2},${this.y2}`;}
    else{
      return `M${this.x1},${this.y1}A0,0 0,0,0 ${this.x2},${this.y2}`;}
  }


  linkFilterDisplay(link : Link) : Boolean {
    return (link.nbCalls >= this._legendService.minLink)
      && (link.nbCalls <= this._legendService.maxLink)
  }

  display(link : Link) {
    return this.linkFilterDisplay(link) && this._nodeVisualService.display(link.source) && this._nodeVisualService.display(link.target)
  }
}
