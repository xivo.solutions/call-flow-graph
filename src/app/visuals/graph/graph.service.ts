import { Injectable } from '@angular/core';
import { Vector } from 'app/d3/models/vector.model';
import { interpolateRgb, piecewise } from 'd3';

@Injectable({
  providedIn: 'root'
})
export class GraphService {

  private get MIN_NODE_SIZE() {return 25};
  private get MAX_NODE_SIZE() {return 80};
  private get MAX_LINK_SIZE() {return 25};
  private get MIN_LINK_SIZE() {return 4};

  constructor() { }

  divide(value : number, maxValue : number, minValue : number){
    if (maxValue - minValue == 0) return 0
    else return (value-minValue)/(maxValue-minValue)
  }

  nodeColorFromCalls(value : number, maxValue : number, minValue: number) : string {
    const interpolate = piecewise(interpolateRgb.gamma(2.2), ["#2dc937", "#99c140", "#e7b416", "#db7b2b", "#cc3232"]);
    return interpolate(this.divide(value, maxValue, minValue))
  }

  nodeSize(value : number, maxValue : number, minValue: number, text : String) : Vector {
    const height : number = Math.max.apply(null, [this.divide(value, maxValue, minValue)*this.MAX_NODE_SIZE, this.MIN_NODE_SIZE]);
    const width : number = Math.max.apply(null, [height, text.length * 6, `${value} users`.length*5]);
    return { x : width , y : height }
  }

  linkSizeFromCalls(value : number, maxValue : number, minValue: number) : number {
    return Math.max.apply(null, [this.divide(value, maxValue, minValue)*this.MAX_LINK_SIZE, this.MIN_LINK_SIZE]);
  }

  linkColorFromCalls(value : number, maxValue : number, minValue: number) : string {
    const interpolate = piecewise(interpolateRgb.gamma(1), ["#2dc937", "#99c140", "#e7b416", "#db7b2b", "#cc3232"])
    return interpolate(this.divide(value, maxValue, minValue))
  }
}
