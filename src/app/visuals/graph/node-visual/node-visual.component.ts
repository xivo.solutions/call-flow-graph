import { Node } from 'app/d3/models/node';
import { Component, Input } from '@angular/core';
import { NodeVisualService } from './node-visual.service';
import d3 from 'd3';

@Component({
  selector: '[nodeVisual]',
  templateUrl : './node-visual.component.html',
  styleUrls: ['./node-visual.component.scss']
})
export class NodeVisualComponent {
  @Input('nodeVisual') node: Node;

  constructor(private _nodeVisualService : NodeVisualService) { }

  get display(){
    return this._nodeVisualService.display(this.node)
  }
  get text_x() {
    return this._nodeVisualService.getTextX(this.node)
  }
  get text_y() {
    return this._nodeVisualService.getTextY(this.node)
  }
  get x() {
    return this._nodeVisualService.getX(this.node)
  }
  get y() {
    return this._nodeVisualService.getY(this.node)
  }
}
