import { TestBed } from '@angular/core/testing';

import { NodeVisualService } from './node-visual.service';

describe('NodeVisualService', () => {
  let service: NodeVisualService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NodeVisualService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
