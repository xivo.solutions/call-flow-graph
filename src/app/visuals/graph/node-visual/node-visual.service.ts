import { Injectable } from '@angular/core';
import { Node } from 'app/d3';
import { LegendService } from 'app/visuals/side-band/legend/legend.service';
import { NodeSelectorService } from 'app/visuals/side-band/node-selector/node-selector.service';

@Injectable({
  providedIn: 'root'
})
export class NodeVisualService {

  constructor(private _nodeSelectorService: NodeSelectorService, private _legendService : LegendService) {}

  getX(node : Node) {
    if (node && node.x && node.isTrunk) {
      return node!.x-node.size.x/2
    }
    if (node && node.x && node.isMds) {
      return node!.x
    }
    return 0
  }

  getY(node : Node) {
    if (node && node.y && node.isTrunk) {
      return node!.y-node.size.y/2
    }
    if (node && node.x && node.isMds) {
      return node!.y
    }
    return 0
  }

  getTextY(node : Node) {
    if (node && node.y && node.isTrunk) {
      return ""
    }
    if (node && node.y && node.isMds) {
      return node.y+20
    }
    return 0
  }

  getTextX(node : Node) {
    if (node && node.x && node.isTrunk) {
      return ""
    }
    if (node && node.x && node.isMds) {
      return node.x-25
    }
    return 0
  }

  // See if the Node needs to be displayed

  NodeSelectorDisplay(node : Node) : Boolean {
    if (node.isTrunk)
      return (this._nodeSelectorService.mdsMap.get(node.mds as String) as Boolean)
    if (node.isMds)
      return (this._nodeSelectorService.mdsMap.get(node.id) as Boolean)
    return false
  }

  NodeFilterDisplay(node : Node) : Boolean {
    if (node.isTrunk)
      return (node.companionMdsUsers as number >= this._legendService.minNode) && (node.companionMdsUsers as number <= this._legendService.maxNode)
    if (node.isMds)
      return node.users <= this._legendService.maxNode && node.users >= this._legendService.minNode
    return false
  }

  LinkFilterDisplay(node : Node) : Boolean {
    if (node.isTrunk)
      return (node.incoming as number >= this._legendService.minLink)
        && (node.incoming as number <= this._legendService.maxLink)
        || (node.outgoing as number >= this._legendService.minLink)
        && (node.outgoing as number <= this._legendService.maxLink)
    if (node.isMds)
      return true
    return false
  }

  display(node : Node) {
    return this.NodeSelectorDisplay(node) && this.NodeFilterDisplay(node) && this.LinkFilterDisplay(node)
  }
}
