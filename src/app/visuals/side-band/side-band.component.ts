import { SideBandService } from './side-band.service';
import { Component, HostBinding, HostListener } from '@angular/core';

@Component({
  selector: 'app-side-band',
  templateUrl: './side-band.component.html',
  styleUrls: ['./side-band.component.scss']
})
export class SideBandComponent {

  /**
  * This stores the state of the resizing event and is updated
  * as events are fired.
  */

  resizingEvent = {
    isResizing: false,
    startingCursorX: 0,
    startingWidth: 0,
  };

  constructor(public sideBandService: SideBandService) {}

  startResizing(event: MouseEvent): void {
    this.resizingEvent = {
      isResizing: true,
      startingCursorX: event.clientX,
      startingWidth: this.sideBandService.sidenavWidth,
    };
  }

  /*
 * This method runs when the mouse is moved anywhere in the browser
 */

@HostListener('window:mousemove', ['$event'])
updateSidenavWidth(event: MouseEvent) {
  // No need to even continue if we're not resizing
  if (!this.resizingEvent.isResizing) {
    return;
  }

  // 1. Calculate how much mouse has moved on the x-axis
  const cursorDeltaX = event.clientX - this.resizingEvent.startingCursorX;

  // 2. Calculate the new width according to initial width and mouse movement
  const newWidth = this.resizingEvent.startingWidth + cursorDeltaX;

  // 3. Set the new width
  this.sideBandService.setSidenavWidth(newWidth);
}

@HostListener('window:mouseup')
stopResizing() {
  this.resizingEvent.isResizing = false;
}

@HostBinding('class.is-expanded')
  get isExpanded() {
    return this.sideBandService.isExpanded;
  }

}
