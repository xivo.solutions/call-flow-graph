import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SideBandComponent } from './side-band.component';

describe('SideBandComponent', () => {
  let component: SideBandComponent;
  let fixture: ComponentFixture<SideBandComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SideBandComponent]
    });
    fixture = TestBed.createComponent(SideBandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
