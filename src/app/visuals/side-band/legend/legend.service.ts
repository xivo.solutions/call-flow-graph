import { Injectable } from '@angular/core';
import { DataImportService } from 'app/API_call/data-import.service';

@Injectable({
  providedIn: 'root'
})
export class LegendService {
  maxValueNode : number;
  maxValueLink : number;
  minValueNode : number;
  minValueLink : number;

  minNode : number;
  minLink : number;
  maxNode : number;
  maxLink : number;

  constructor(private _dataImportservice : DataImportService) {
    this._dataImportservice.subscribeToExtremums().subscribe( extremums => {
      this.maxValueNode = extremums.maxUsers;
      this.maxNode = this.maxValueNode;

      this.minValueNode = extremums.minUsers;
      this.minNode = this.minValueNode;

      this.maxValueLink = extremums.maxLinkCalls;
      this.maxLink = this.maxValueLink;

      this.minValueLink = extremums.minLinkCalls;
      this.minLink = this.minValueLink;
    });
  }
}
