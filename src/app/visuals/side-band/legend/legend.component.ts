import { Component } from '@angular/core';
import { LegendService } from './legend.service';

@Component({
    selector: 'app-legend',
    templateUrl : './legend.component.html',
    styleUrls: ['./legend.component.scss']
})
export class LegendComponent{

  constructor(private _legendService : LegendService) {}

  formatLabel(value: number): string {

    if (value >= 1000) {
      const partieEntiere = Math.round(value / 1000);
      const partieDecimale = Math.round(( value - partieEntiere * 1000 ) / 1000)

      return  `${partieEntiere}.${partieDecimale}k`;
    }
    return value.toString();

  }

  get maxValueNode() : number {
    return this._legendService.maxValueNode
  }

  get maxValueLink() : number {
    return this._legendService.maxValueLink
  }

  get minValueNode(){
    return this._legendService.minValueNode
  }

  get minValueLink(){
    return this._legendService.minValueLink
  }
  get minNode() {
    return this._legendService.minNode;
  }
  get minLink() {
    return this._legendService.minLink;
  }
  get maxNode() {
    return this._legendService.maxNode;
  }
  get maxLink() {
    return this._legendService.maxLink;
  }


  set minNode(value : number) {
    this._legendService.minNode = value
  }
  set minLink(value : number) {
    this._legendService.minLink = value
  }
  set maxNode(value : number) {
    this._legendService.maxNode = value
  }
  set maxLink(value : number) {
    this._legendService.maxLink = value
  }

}
