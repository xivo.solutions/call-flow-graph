import { Component } from '@angular/core';
import { NodeSelectorService } from './node-selector.service';
import { MdsData } from './mdsData.model';


@Component({
  selector: 'app-node-selector',
  templateUrl: './node-selector.component.html',
  styleUrls: ['./node-selector.component.scss'],
})
export class NodeSelectorComponent {
  search : string ="";
  allDisplayed : Boolean

  constructor(private _nodeSelectorService : NodeSelectorService) {
    this.allDisplayed=true
  }

  set mdsList (mdsList : MdsData[]) {
    this._nodeSelectorService.mdsList = mdsList
  }

  get mdsList() : MdsData[]{
    let ListOfMds = this._nodeSelectorService.mdsList;
    if (ListOfMds) {
      ListOfMds.sort((a, b) => a.name.localeCompare(b.name, "fr"));
    }
    return ListOfMds;
  }

  updateListOfMds(){
    let ListOfMds = this._nodeSelectorService.mdsList;
    if (ListOfMds) {
      ListOfMds.sort((a, b) => a.name.localeCompare(b.name, "fr"));
    }
  }


  updateAllDisplayed(){
    this.allDisplayed = this.mdsList.every(t => t.displayed);
  }


  someComplete(): boolean {
    return this.mdsList.filter(mds => mds.displayed).length > 0 && !this.allDisplayed;
  }

  setAll(checked : boolean){
    this.allDisplayed = checked
    this.mdsList.forEach(mds=>mds.displayed=this.allDisplayed)
  }
  updateMdsList(){
    this._nodeSelectorService.updateMdsList(this.mdsList)
  }
}

