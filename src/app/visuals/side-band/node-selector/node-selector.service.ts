import { Injectable } from '@angular/core';
import { DataImportService } from 'app/API_call/data-import.service';
import { Import, Mds } from 'app/API_call/models';
import { BehaviorSubject } from 'rxjs';
import { MdsData } from './mdsData.model';

@Injectable({
  providedIn: 'root'
})
export class NodeSelectorService {
  mdsList : MdsData[] = [];
  private _mdsList$ = new BehaviorSubject<MdsData[]>([]);

  constructor(private _dataImportService : DataImportService) {

    this._dataImportService.subscribeToImport().subscribe((data : Import | undefined) => {
      if (data && data.mdsList) {
        this.mdsList=[]
        data.mdsList.forEach((mds : Mds)=>{
          this.mdsList.push({id : mds.name, name : mds.display_name, displayed : true});
        });
      }
    });

  }

  updateMdsList(mdsList : MdsData[]) {
    this._mdsList$.next(mdsList)
  }

  subsribeToMdsList() {
    return this._mdsList$.asObservable()
  }

  get mdsMap() : Map<String, Boolean> {
    return new Map(this.mdsList.map(mds=>[mds.id, mds.displayed]))
  }
}
