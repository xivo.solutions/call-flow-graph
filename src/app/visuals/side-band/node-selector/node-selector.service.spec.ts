import { TestBed } from '@angular/core/testing';

import { NodeSelectorService } from './node-selector.service';

describe('NodeSelectorService', () => {
  let service: NodeSelectorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NodeSelectorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
