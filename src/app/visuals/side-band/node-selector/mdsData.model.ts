export interface MdsData {
  id : string;
  name : string;
  displayed : Boolean;
}
