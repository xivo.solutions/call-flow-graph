import { DataImportService } from '../../../API_call/data-import.service';
import {Component} from '@angular/core';
import {FormGroup, FormsModule, ReactiveFormsModule, FormBuilder} from '@angular/forms';
import {NgIf, JsonPipe} from '@angular/common';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatNativeDateModule} from '@angular/material/core';


@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatDatepickerModule,
    FormsModule,
    ReactiveFormsModule,
    NgIf,
    JsonPipe,
    MatNativeDateModule,
  ]
})
export class DatePickerComponent {
  range: FormGroup;

  constructor(private _dataImportService : DataImportService, private _formBuilder : FormBuilder) {
    const currentDate = new Date
    this.range = this._formBuilder.group({
      end: new Date(),
      start: new Date(currentDate.setDate(currentDate.getDate() - currentDate.getDay())),

    });

    this.request(this.range.value)

    // Subscribe to the valueChanges event of the FormGroup

    this.range.valueChanges.subscribe((value) => {
      this.request(value)
    });
  }

  private request(value : any){
    if (value != null && value.start != null && value.end != null) {
      const startDate = value.start.toISOString().replace('T', ' ').replace('Z','');
      const endDate = value.end.toISOString().replace('T', ' ').replace('Z','');
      this._dataImportService.requestDate(startDate, endDate);
    }
  }
}
