/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SideBandService } from './side-band.service';

describe('Service: SideBand', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SideBandService]
    });
  });

  it('should ...', inject([SideBandService], (service: SideBandService) => {
    expect(service).toBeTruthy();
  }));
});
