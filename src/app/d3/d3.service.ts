import { Injectable } from '@angular/core';
import { Link } from './models/link';
import { ForceDirectedGraph } from './models/force-directed-graph';
import { Node } from './models/node';
import * as d3 from 'd3';

@Injectable({
  providedIn: 'root'
})
export class D3Service {

  constructor() {}

  /** A method to bind a pan and zoom behaviour to an svg element */
  applyZoomableBehaviour(svgElement : Element, containerElement : Element) {
    let svg: d3.Selection<Element, unknown, null, undefined>,
        container: d3.Selection<Element, unknown, null, undefined>,
        zoomed: (event: d3.D3ZoomEvent<Element, unknown>) => void,
        zoom: d3.ZoomBehavior<Element, unknown>;

    svg = d3.select(svgElement);
    container = d3.select(containerElement);

    zoomed = (event : d3.D3ZoomEvent<Element, unknown>) => {
      const transform = event.transform;
      container.attr('transform', 'translate(' + transform.x + ',' + transform.y + ') scale(' + transform.k + ')');
    }

    zoom = d3.zoom().on('zoom', zoomed);
    svg.call(zoom);
  }

/** A method to bind a draggable behaviour to an svg element */
applyDraggableBehaviour(element : Element, node: Node, graph: ForceDirectedGraph) {
  const d3element = d3.select(element);

  function started(event : d3.D3DragEvent<SVGElement, Node, Node>) {
    /** Preventing propagation of dragstart to parent elements */
    event.sourceEvent.stopPropagation();

    if (!event.active) {
      graph?.simulation?.alphaTarget(0.3).restart();
    }

    event.on('drag', dragged).on('end', ended);

    function dragged(event : d3.D3DragEvent<SVGElement, Node, Node>) {
      node.fx = event.x;
      node.fy = event.y;
    }

    function ended(event : d3.D3DragEvent<SVGElement, Node, Node>) {
      if (!event.active) {
        graph?.simulation?.alphaTarget(0);
      }

      node.fx = null;
      node.fy = null;
    }
  }

  d3element.call(d3.drag()
    .on('start', started));
}
    getForceDirectedGraph(nodes: Node[], links: Link[]) {
      let graph = new ForceDirectedGraph(nodes, links);
      return graph;
    }
}
