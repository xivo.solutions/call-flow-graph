import * as d3 from 'd3';
import { Vector } from './vector.model';

// Implementing SimulationNodeDatum interface into our custom Node class
export class Node implements d3.SimulationNodeDatum {
  // Optional - defining optional implementation properties - required for relevant typing ;
  x?: number;
  y?: number;
  vx?: number;
  vy?: number;
  fx?: number | null;
  fy?: number | null;

  id: string;
  text:string;
  size: any = 0;
  color : string;
  shape : 'rectangle' | 'ellipse';
  users : number;
  incoming ?: number;
  outgoing ?: number;
  mds ?: String;
  companionMdsUsers ?: number;


  constructor(id : string, text : string, shape : 'rectangle' | 'ellipse', color : string, size : Vector, users : number, incoming?: number, outgoing?: number, mds?: String, companionMdsUsers?: number) {
      this.id = id;
      this.text = text;
      this.shape = shape;
      this.color = color;
      this.size = size;
      this.users = users;
      this.incoming = incoming;
      this.outgoing = outgoing;
      this.mds = mds;
      this.companionMdsUsers = companionMdsUsers;
  }

  get isMds() {
    return this.shape == 'ellipse'
  }

  get isTrunk() {
    return this.shape == 'rectangle'
  }
}
