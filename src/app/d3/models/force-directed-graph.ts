import { EventEmitter } from '@angular/core';
import { Link } from './link';
import { Node } from './node';
import * as d3 from 'd3';
import { SimulationNodeDatum } from 'd3';
import { Vector } from './vector.model';
const FORCES = {
    LINKS: 2,
    CHARGE: -15
}
const MIN_NODE_DISTANCE = 50;
const LINK_DISTANCE = 350;
export class ForceDirectedGraph {
    public ticker: EventEmitter<d3.Simulation<d3.SimulationNodeDatum, d3.SimulationLinkDatum<d3.SimulationNodeDatum> | undefined>> = new EventEmitter();
    public simulation?: d3.Simulation<d3.SimulationNodeDatum, d3.SimulationLinkDatum<d3.SimulationNodeDatum> | undefined>;

    public nodes: Node[] = [];
    public links: Link[] = [];
    public svg = d3.select("svg")
    private width  = parseInt(this.svg.style("width"));
    private height = parseInt(this.svg.style("height"));

    offsetVector(index : number, nodeNumber : number) : Vector {
      return {
        x : 0,
        y : 0
      }
    }

    constructor(nodes : Node[], links: Link[]) {

        this.nodes = nodes;
        this.links = links;
        const nodeNumber = this.nodes.length
        this.nodes.forEach((node : Node, i : number) => {
          const offsetVector = this.offsetVector(i, nodeNumber)
          node.x = this.width/2 + offsetVector.x;
          node.y = this.height/2 + offsetVector.y;
        });
        this.initSimulation();
    }

    initNodes() {
        if (!this.simulation) {
            throw new Error('simulation was not initialized yet');
        }

        this.simulation.nodes(this.nodes);
    }

    initLinks() {
        if (!this.simulation) {
            throw new Error('simulation was not initialized yet');
        }

        // Initializing the links force simulation

        this.simulation.force('links',
            d3.forceLink(this.links)
                .strength(FORCES.LINKS)
                .distance(LINK_DISTANCE)
        );
    }
    getCollisionDistance(nodeDatum: SimulationNodeDatum, i: number, nodes: SimulationNodeDatum[]) : number {
      const node = nodeDatum as Node
      if (node.isMds) return Math.max.apply(null, [node.size.x, node.size.y])
      if (node.isTrunk) return (node.size.x+node.size.y)/4
      return 0
    }

    initSimulation() {
        /** Creating the simulation */
        if (!this.simulation) {
            const ticker = this.ticker;

            // Creating the force simulation and defining the charges
            this.simulation = d3.forceSimulation()
            .force("charge",
                d3.forceManyBody()
                    .strength(FORCES.CHARGE)
            )
            .force('collision', d3.forceCollide().radius(this.getCollisionDistance));

            // Connecting the d3 ticker to an angular event emitter
            this.simulation.on('tick', function () {
                ticker.emit(this);
            });

            this.initNodes();
            this.initLinks();
        }


        /** Restarting the simulation internal timer */
        this.simulation.restart();
    }
}
