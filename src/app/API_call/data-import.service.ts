import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, map , of} from 'rxjs';
import { Import, Mds, Trunk, Calls } from './models';


interface Extremums {
  maxNodeCalls : number;
  minNodeCalls : number;
  maxLinkCalls : number;
  minLinkCalls : number;
  maxUsers : number;
  minUsers : number;
}

@Injectable({
  providedIn: 'root'
})

export class DataImportService {

  private _hostName: string = 'http://localhost:9000';
  private _import$ = new BehaviorSubject<Import | undefined>(undefined);

  private _extremums$ = new BehaviorSubject<Extremums>({maxNodeCalls : 0, minNodeCalls : 0, maxLinkCalls : 0, minLinkCalls : 0, maxUsers : 0, minUsers : 0});

  constructor(private _httpClient: HttpClient) {}


  requestDate(date1 : string, date2 : string) {
    /*const data = {mdsList:[
      {name: 'default', display_name: 'a', users : 2000, calls : 10},
      {name: 'mds1', display_name: 'b', users : 2000, calls : 100},
      {name: 'mds2', display_name: 'jijof', users : 10, calls : 600},
      {name: 'mds3', display_name: 'egg', users : 34, calls : 900},
      {name: 'mds4', display_name: '  &féf', users : 12, calls : 89},
      {name: 'mds5', display_name: 'egéa', users : 200, calls : 561},
      {name: 'mds6', display_name: 'éf  zd', users : 563, calls : 561},
      {name: 'mds7', display_name: 'htrhr', users : 900, calls : 561},
      {name: 'mds8', display_name: 'zeea  ', users : 200, calls : 561},
      {name: 'mds9', display_name: 'dhuejai', users : 982, calls : 561},
      {name: 'mds11', display_name: 'fezetg', users : 32, calls : 561},
      {name: 'mds12', display_name: 'dhuejai', users : 982, calls : 561},
      {name: 'mds13', display_name: 'fezetg', users : 32, calls : 561}
      ],
      callDatasList:[
      {caller_mds : 'default', callee_mds : 'mds1', calls : 1000},
      {caller_mds : 'mds1', callee_mds : 'default', calls : 100},
      {caller_mds : 'mds5', callee_mds : 'mds1', calls : 600},
      {caller_mds : 'default', callee_mds : 'default', calls : 189},
      {caller_mds : 'mds6', callee_mds : 'mds6', calls : 842},
      {caller_mds : 'mds6', callee_mds : 'mds2', calls : 45},
      {caller_mds : 'mds2', callee_mds : 'mds2', calls : 900}
      ],
      trunkList:[
       {name : 'trunk 1', incoming : 7, outgoing : 2, mds : 'default'},
       {name : 'trunk 2', incoming : 763, outgoing : 12, mds : 'default'},
       {name : 'trunk 3', incoming : 96, outgoing : 584, mds : 'mds1'},
       {name : 'trunk ckevjzfeffdze', incoming : 456, outgoing : 1200, mds : 'mds4'},
      ]}*/

     this._httpClient.post<Import>(`${this._hostName}/CallFlows`, {lowerBound : date1, upperBound : date2}).subscribe((data : Import | undefined) => {
       if (data){
        this._import$.next(data)

        const users = data.mdsList.map((mds : Mds) => mds.users)
        const maxUsers = (users.length == 0) ? 0 : Math.max.apply(null, users);
        const minUsers = (users.length == 0) ? 0 : Math.min.apply(null, users);

        const nodeCalls = data.mdsList.map((mds : Mds) => mds.calls).concat(data.trunkList.map( (trunk : Trunk) => trunk.incoming + trunk.outgoing))
        const maxNodeCalls =  (nodeCalls.length == 0) ? 0 : Math.max.apply(null, nodeCalls);
        const minNodeCalls =  (nodeCalls.length == 0) ? 0 : Math.min.apply(null, nodeCalls);

        const linkCalls = data.trunkList.map((trunk : Trunk) => [trunk.incoming, trunk.outgoing]).flat().concat(data.callDatasList.map((call : Calls)=>call.calls));
        const maxLinkCalls = (linkCalls.length == 0) ? 0 : Math.max.apply(null, linkCalls);
        const minLinkCalls = (linkCalls.length == 0) ? 0 : Math.min.apply(null, linkCalls);

        this._extremums$
        .next({
          maxNodeCalls : maxNodeCalls,
          minNodeCalls : minNodeCalls,
          maxLinkCalls : maxLinkCalls,
          minLinkCalls : minLinkCalls,
          maxUsers : maxUsers,
          minUsers : minUsers
        })

      }
     })
   }

  subscribeToImport(): Observable<Import | undefined> {
    return this._import$.asObservable();
  }

  subscribeToExtremums() : Observable<Extremums> {
    return this._extremums$.asObservable();
  }

}

