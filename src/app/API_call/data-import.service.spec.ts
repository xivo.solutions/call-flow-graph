/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DataImportService } from './data-import.service';

describe('Service: DataImport', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataImportService]
    });
  });

  it('should ...', inject([DataImportService], (service: DataImportService) => {
    expect(service).toBeTruthy();
  }));
});
