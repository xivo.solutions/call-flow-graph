import { Calls } from "./calls";
import { Mds } from "./mds";
import { Trunk } from "./trunk";

export interface Import {
  mdsList: Array<Mds>;
  callDatasList: Array<Calls>;
  trunkList: Array<Trunk>;
}
