export interface Trunk {
  name: string;
  incoming : number;
  outgoing : number;
  mds: string;
}
