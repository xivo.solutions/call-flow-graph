export interface Mds {
  name: string;
  display_name: string;
  users: number;
  calls: number;
}

